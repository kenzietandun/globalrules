﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CodeActions;
using Microsoft.CodeAnalysis.CodeFixes;
using Microsoft.CodeAnalysis.CSharp;
using System.Collections.Immutable;
using System.Composition;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using GlobalRules;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using static Microsoft.CodeAnalysis.CSharp.SyntaxFactory;

namespace GlobalRules
{
    [ExportCodeFixProvider(LanguageNames.CSharp, Name = nameof(LinqQueryMissingBracketsCodeFixProvider)), Shared]
    public class LinqQueryMissingBracketsCodeFixProvider : CodeFixProvider
    {
        public sealed override ImmutableArray<string> FixableDiagnosticIds => ImmutableArray.Create(LinqQueryMissingBracketsAnalyzer.DiagnosticId);

        public sealed override FixAllProvider GetFixAllProvider()
        {
            return WellKnownFixAllProviders.BatchFixer;
        }

        public sealed override async Task RegisterCodeFixesAsync(CodeFixContext context)
        {
            var root = await context.Document.GetSyntaxRootAsync(context.CancellationToken).ConfigureAwait(false);
            var diagnostic = context.Diagnostics.First();
            var diagnosticSpan = diagnostic.Location.SourceSpan;

            var syntax = root.FindToken(diagnosticSpan.Start)
                .Parent
                .AncestorsAndSelf()
                .OfType<LocalDeclarationStatementSyntax>()
                .FirstOrDefault();

            if (syntax != null)
            {
                context.RegisterCodeFix(
                    CodeAction.Create(
                        title: CodeFixResources.MultipleNewlinesCodeFixTitle,
                        createChangedDocument: c => Fix(context.Document, syntax, c),
                        equivalenceKey: CodeFixResources.MultipleNewlinesCodeFixTitle),
                    diagnostic);
            }
        }

        private async Task<Document> Fix(Document document, LocalDeclarationStatementSyntax statement, CancellationToken cancellationToken)
        {
            var root = await document.GetSyntaxRootAsync(cancellationToken).ConfigureAwait(false);

            var queryExpression = statement
                .DescendantNodes()
                .OfType<QueryExpressionSyntax>()
                .First();

            root = root.ReplaceNode(
                queryExpression,
                ParenthesizedExpression(queryExpression)
                    .WithOpenParenToken(
                        Token(
                            TriviaList(),
                            SyntaxKind.OpenParenToken,
                            TriviaList(
                                LineFeed))));

            var equalsToken = root
                .DescendantTokens()
                .First(dt => dt.IsKind(SyntaxKind.EqualsToken));

            root = root.ReplaceToken(
                equalsToken,
                equalsToken.WithTrailingTrivia(TriviaList(Whitespace(" "))));
            
            return document.WithSyntaxRoot(root);
        }
    }
}