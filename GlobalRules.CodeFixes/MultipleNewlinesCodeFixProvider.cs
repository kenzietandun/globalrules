﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CodeActions;
using Microsoft.CodeAnalysis.CodeFixes;
using Microsoft.CodeAnalysis.CSharp;
using System.Collections.Immutable;
using System.Composition;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using GlobalRules;

namespace GlobalRules
{
    [ExportCodeFixProvider(LanguageNames.CSharp, Name = nameof(MultipleNewlinesCodeFixProvider)), Shared]
    public class MultipleNewlinesCodeFixProvider : CodeFixProvider
    {
        public sealed override ImmutableArray<string> FixableDiagnosticIds => ImmutableArray.Create(MultipleNewlinesAnalyzer.DiagnosticId);

        public sealed override FixAllProvider GetFixAllProvider()
        {
            return WellKnownFixAllProviders.BatchFixer;
        }

        public sealed override async Task RegisterCodeFixesAsync(CodeFixContext context)
        {
            var root = await context.Document.GetSyntaxRootAsync(context.CancellationToken).ConfigureAwait(false);
            var diagnostic = context.Diagnostics.First();
            var diagnosticSpan = diagnostic.Location.SourceSpan;

            var syntax = root.FindToken(diagnosticSpan.Start);

            context.RegisterCodeFix(
                CodeAction.Create(
                    title: CodeFixResources.MultipleNewlinesCodeFixTitle,
                    createChangedDocument: c => Fix(context.Document, syntax, c),
                    equivalenceKey: CodeFixResources.MultipleNewlinesCodeFixTitle),
                diagnostic);
        }

        private async Task<Document> Fix(Document document, SyntaxToken token, CancellationToken cancellationToken)
        {
            var root = await document.GetSyntaxRootAsync(cancellationToken).ConfigureAwait(false);

            var newToken = token
                .WithoutTrivia()
                .WithTrailingTrivia(new[]
                {
                    SyntaxFactory.EndOfLine(string.Empty)
                });

            return document.WithSyntaxRoot(root.ReplaceToken(token, newToken));
        }
    }
}