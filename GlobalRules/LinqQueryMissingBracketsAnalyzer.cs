﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Diagnostics;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Threading;

namespace GlobalRules
{
    [DiagnosticAnalyzer(LanguageNames.CSharp)]
    public class LinqQueryMissingBracketsAnalyzer : DiagnosticAnalyzer
    {
        public const string DiagnosticId = "Global_LinqQueryMustHaveBrackets";
        private const string Category = "Structure";

        private static readonly LocalizableString Title = new LocalizableResourceString(
            nameof(Resources.MultipleNewlinesAnalyzerTitle), 
            Resources.ResourceManager, typeof(Resources));

        private static readonly LocalizableString MessageFormat = new LocalizableResourceString(
            nameof(Resources.MultipleNewlinesAnalyzerMessageFormat), 
            Resources.ResourceManager, typeof(Resources));

        private static readonly LocalizableString Description = new LocalizableResourceString(
            nameof(Resources.MultipleNewlinesAnalyzerDescription), 
            Resources.ResourceManager, typeof(Resources));

        private static readonly DiagnosticDescriptor Rule = new DiagnosticDescriptor(
            DiagnosticId, 
            Title, 
            MessageFormat, 
            Category, 
            DiagnosticSeverity.Warning, 
            isEnabledByDefault: true, 
            description: Description);

        public override ImmutableArray<DiagnosticDescriptor> SupportedDiagnostics => ImmutableArray.Create(Rule);

        public override void Initialize(AnalysisContext context)
        {
            context.ConfigureGeneratedCodeAnalysis(GeneratedCodeAnalysisFlags.None);
            context.EnableConcurrentExecution();
            context.RegisterSyntaxTreeAction(AnalyzeLinqQueries);
        }

        private void AnalyzeLinqQueries(SyntaxTreeAnalysisContext context)
        {
            var root = context.Tree.GetRoot(context.CancellationToken);

            var linqWithoutBrackets = root.DescendantNodes()
                .Where((node) =>
                    node.IsKind(SyntaxKind.QueryExpression)
                    && !node.Parent.IsKind(SyntaxKind.ParenthesizedExpression));

            foreach (var violation in linqWithoutBrackets)
            {
                context.ReportDiagnostic(Diagnostic.Create(Rule, violation.GetLocation()));
            }
        }
    }
}
