﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Diagnostics;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Threading;

namespace GlobalRules
{
    [DiagnosticAnalyzer(LanguageNames.CSharp)]
    public class MultipleNewlinesAnalyzer : DiagnosticAnalyzer
    {
        public const string DiagnosticId = "Global_MultipleNewlinesNotAllowed";
        private const string Category = "Structure";

        private static readonly LocalizableString Title = new LocalizableResourceString(
            nameof(Resources.MultipleNewlinesAnalyzerTitle), 
            Resources.ResourceManager, typeof(Resources));

        private static readonly LocalizableString MessageFormat = new LocalizableResourceString(
            nameof(Resources.MultipleNewlinesAnalyzerMessageFormat), 
            Resources.ResourceManager, typeof(Resources));

        private static readonly LocalizableString Description = new LocalizableResourceString(
            nameof(Resources.MultipleNewlinesAnalyzerDescription), 
            Resources.ResourceManager, typeof(Resources));

        private static readonly DiagnosticDescriptor Rule = new DiagnosticDescriptor(
            DiagnosticId, 
            Title, 
            MessageFormat, 
            Category, 
            DiagnosticSeverity.Warning, 
            isEnabledByDefault: true, 
            description: Description);

        public override ImmutableArray<DiagnosticDescriptor> SupportedDiagnostics => ImmutableArray.Create(Rule);

        public override void Initialize(AnalysisContext context)
        {
            context.ConfigureGeneratedCodeAnalysis(GeneratedCodeAnalysisFlags.None);
            context.EnableConcurrentExecution();
            context.RegisterSyntaxTreeAction(AnalyzeNewlines);
        }

        private void AnalyzeNewlines(SyntaxTreeAnalysisContext context)
        {
            var root = context.Tree.GetRoot(context.CancellationToken);

            var newlines = root.DescendantTrivia()
                .Where((node) =>
                    node.IsKind(SyntaxKind.EndOfLineTrivia)
                    && node.GetLocation().GetLineSpan().StartLinePosition.Character == 0);

            if (newlines != null)
            {
                CheckNewlinesRulesViolation(context, newlines);
            }
        }

        private void CheckNewlinesRulesViolation(SyntaxTreeAnalysisContext context, IEnumerable<SyntaxTrivia> newlines)
        {
            var totalNewlines = newlines.Count();
            var reportedLines = new HashSet<int>();

            for (var i = 0; i < totalNewlines - 1; i++)
            {
                var (newline, nextNewline) = (
                    newlines.ElementAt(i), 
                    newlines.ElementAt(i + 1));

                var (newlineLineNumber, nextNewlineLineNumber) = (
                    newline.GetLocation().GetLineSpan().StartLinePosition.Line,
                    nextNewline.GetLocation().GetLineSpan().StartLinePosition.Line);

                if (newlineLineNumber + 1 == nextNewlineLineNumber
                    && !reportedLines.Contains(newlineLineNumber)
                    && !reportedLines.Contains(nextNewlineLineNumber))
                {
                    context.ReportDiagnostic(Diagnostic.Create(Rule, newline.GetLocation()));

                    reportedLines.Add(newlineLineNumber);
                    reportedLines.Add(nextNewlineLineNumber);
                }
            }
        }
    }
}
