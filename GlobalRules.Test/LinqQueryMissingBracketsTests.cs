﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;
using VerifyCS = GlobalRules.Test.CSharpCodeFixVerifier<
    GlobalRules.LinqQueryMissingBracketsAnalyzer,
    GlobalRules.LinqQueryMissingBracketsCodeFixProvider>;

namespace GlobalRules.Test
{
    [TestClass]
    public class LinqQueryMissingBracketsTests
    {
        [TestMethod]
        public async Task NoDiagnosticsTest()
        {
            const string test = @"
            using System;
            using System.Collections.Generic;
            using System.Linq;
            using System.Text;
            using System.Threading.Tasks;
            using System.Diagnostics;

            namespace ConsoleApplication1
            {
                class Test
                {   
                    void Method()
                    {
                        var test = (
                            from x in Enumerable.Range(1, 10)
                            select x);
                    }
                }
            }
            ";

            await VerifyCS.VerifyAnalyzerAsync(test);
        }

        //Diagnostic and CodeFix both triggered and checked for
        [TestMethod]
        public async Task CodeFixTest()
        {
            const string test = @"
            using System;
            using System.Linq;

            namespace ConsoleApplication1
            {
                class Test
                {   
                    void Method()
                    {
                        var test = 
                            {|#0:from x in Enumerable.Range(1, 10)
                            select x|};
                    }
                }
            }
            ";

            const string textFix = @"
            using System;
            using System.Linq;

            namespace ConsoleApplication1
            {
                class Test
                {   
                    void Method()
                    {
                        var test = (
                            from x in Enumerable.Range(1, 10)
                            select x);
                    }
                }
            }
            ";

            var expected = VerifyCS
                .Diagnostic(LinqQueryMissingBracketsAnalyzer.DiagnosticId)
                .WithLocation(0);
            
            await VerifyCS.VerifyCodeFixAsync(test, expected, textFix);
        }
    }
}