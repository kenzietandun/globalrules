﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;
using VerifyCS = GlobalRules.Test.CSharpCodeFixVerifier<
    GlobalRules.MultipleNewlinesAnalyzer,
    GlobalRules.MultipleNewlinesCodeFixProvider>;

namespace GlobalRules.Test
{
    [TestClass]
    public class MultipleNewlinesTests
    {
        [TestMethod]
        public async Task NoDiagnosticsTest()
        {
            const string test = @"
            using System;

            namespace ConsoleApplication1
            {
                class Test
                {   
                    void Method() { }
                }
            }
            ";

            await VerifyCS.VerifyAnalyzerAsync(test);
        }

        //Diagnostic and CodeFix both triggered and checked for
        [TestMethod]
        public async Task CodeFixTest()
        {
            const string test = @"
            using System;
            {|#0|}

            namespace ConsoleApplication1
            {
                class Test
                {   
                    void Method() { }
                }
            }
            ";

            const string textFix = @"
            using System;

            namespace ConsoleApplication1
            {
                class Test
                {   
                    void Method() { }
                }
            }
            ";

            var expected = VerifyCS
                .Diagnostic(MultipleNewlinesAnalyzer.DiagnosticId)
                .WithLocation(0);
            
            await VerifyCS.VerifyCodeFixAsync(test, expected, textFix);
        }
    }
}